\documentclass[]{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{pgfplots}
\usepackage{multicol}
\usepackage{tikz}
%opening
\title{Ripasso Programma}
\author{Paolo Galletta}
\date{13/07/18}
\pgfplotsset{width=5cm,compat=1.9} 

\begin{document}

\maketitle

\section{Funzioni}

\subsection{Definizione} 
Una Funzione è una relazione tra due insiemi, $\mathbb{A}$ e $\mathbb{B}$, che agisce assegnando ad un valore dell'insieme $\mathbb{A}$ un valore dell'insieme $\mathbb{B}$.

In linguaggio matematico formale, una funzione è definita come:

\begin{align*}
f :& \mathbb{A} \rightarrow \mathbb{B} \\
&x \mapsto f(x)
\end{align*}

\subsection{Terminologia}
Ciascun elemento che compone una funzione possiede un nome formale che è necessario tenere a mente per comprendere appieno quello che si esegue per svolgere un esercizio.

Una funzione è dunque composta da:

\begin{itemize}
	\item $\mathbb{A}$ (Insieme di partenza) viene detto \textbf{Dominio}
	\item $\mathbb{B}$ (Insieme d'arrivo) viene detto \textbf{Codominio}
	\item $f(x)$ è l'\textbf{immagine} di x tramite $f$ ed è un insieme definito come:
	\[ im(f)= f(x) := \{f(x) \in B \arrowvert x \in \mathbb{A}\} \] 

\end{itemize}

\subsection{Funzione Reale}
$f:\mathbb{A} \rightarrow \mathbb{B}$ è \textbf{reale} $\leftrightarrows \mathbb{A},\mathbb{B} \subseteq \mathbb{R}$.

\subsection{Funzione Iniettiva}
$f: \mathbb{R} \rightarrow \mathbb{R} $ è \textbf{iniettiva} $\rightleftarrows \forall x_{1},x_{2} \in \mathbb{R}$ allora $f(x_{1}) \neq f(x_{2})$ 
\subsubsection{Esempi di Funzioni Iniettive}
In questa sotto-sezione verranno mostrate alcune funzioni, con annesso grafico, per meglio comprendere il significato di iniettività di una funzione.
\begin{enumerate}
	\item $y=x$ è una funzione iniettiva
	\item $y=x^3$ è una funzione iniettiva
	\item $y=\ln(x)$ è una funzione iniettiva
	\item $y=e^x$ è una funzione iniettiva
	\item $y=\cos(x)$ \textbf{\underline{NON}} è una funzione iniettiva
	\item $y=\sin(x)$ \textbf{\underline{NON}} è una funzione iniettiva
	\item $y=x^2$ \textbf{\underline{NON}} è una funzione iniettiva
\end{enumerate}

\subsection{Funzioni Suriettive}
$f: \mathbb{R} \rightarrow \mathbb{R} $ è \textbf{suriettiva} $\leftrightarrows \forall \, b \in \mathbb{B}, \; \exists \; a \in \mathbb{A} \, \arrowvert f(a)=b $

\subsubsection{Esempi di Funzioni Suriettive}
\begin{enumerate}
	\item $y=x$ è una funzione \textbf{suriettiva}
	\item $y= \ln(x)$ è una funzione \textbf{suriettiva}
	\item $y=x^2$ è una funzione \textbf{\underline{NON}} è una funzione suriettiva, infatti i valori di $f(x)$ non sono mai negativi
\end{enumerate}

\subsection{Funzioni Biettive}
$f: \mathbb{R} \rightarrow \mathbb{R} $ è \textbf{biettiva} $\leftrightarrows \, f$ è sia \textbf{ iniettiva} che \textbf{suriettiva} 
\subsubsection{Funzioni Inverse}
Sia $f: \mathbb{A}\subseteq  \mathbb{R} \rightarrow  \mathbb{B} \subseteq  \mathbb{R} $ una funzione \textbf{biettiva}, allora esiste la funzione inversa di una tale funzione, definita come:

\begin{align*}
f^{-1}:& \mathbb{B} \rightarrow \mathbb{A}  \\
&b \mapsto x \arrowvert f(x) = b
\end{align*}

\subsubsection{Esempi di Funzioni Invertibili}
\begin{enumerate}
	\item $\cos(x)$ ha come inversa $\arccos(x)$
	\item $\sin(x)$ ha come inversa $\arcsin(x)$
	\item $e^x$ ha come inversa $\ln(x)$
\end{enumerate}
\subsection{Funzioni Pari}
Una funzione $f: \mathbb{A}\subset \mathbb{R} \rightarrow \mathbb{B} \subset \mathbb{R}$ viene detta \textbf{pari} $\rightleftarrows f(-x) = f(x)$.
Tutte le funzioni pari sono \textbf{simmetriche rispetto all'asse delle y}.
\subsubsection{Esempi di funzioni pari}
\begin{enumerate}
	\item $y=x^2$ è una funzione pari
	\item $y=\cos(x)$ è una funzione pari
	\item $y= e^x$ \textbf{\underline{non}} è una funzione pari
	\item $\sqrt{x^2-2}$ è una funzione pari
\end{enumerate}

\subsection{Funzioni dispari}
Una funzione $f: \mathbb{A}\subset \mathbb{R} \rightarrow \mathbb{B} \subset \mathbb{R}$ viene detta \textbf{dispari} $\rightleftarrows f(-x) = -f(x)$.
Tutte le funzioni dispari sono \textbf{simmetriche rispetto all'origine degli assi}.

\subsubsection{Esempi di Funzioni Dispari}
\begin{enumerate}
	\item $y=x$ è una funzione dispari
	\item $\sin(x)$ è una funzione dispari
	\item $y = x^3$ è una funzione dispari
	\item $y = e^x$ \textbf{\underline{non}} è una funzione dispari
\end{enumerate}

\subparagraph{Attenzione:} Le funzioni non sono necessariamente o pari o dispari; esistono infatti delle funzioni che non sono né pari né dispari.

La funzione $\mathbf{y=e^x}$, ad esempio, non è \textbf{né pari né dispari}.

\subsection{Funzioni Superiormente Limitate}
Una funzione $f: \mathbb{A}\subset \mathbb{R} \rightarrow \mathbb{B} \subset \mathbb{R}$ si dice \textbf{superiormente limitata} 

$\leftrightarrows \exists M \in \mathbb{B} \arrowvert f(x) \leq M \, , \forall \, x\in \mathbb{A}$

\subsection{Funzioni Inferiormente Limitate}
Una funzione $f: \mathbb{A}\subset \mathbb{R} \rightarrow \mathbb{B} \subset\leqslant \mathbb{R}$ si dice \textbf{inferiormente limitata} 

$\leftrightarrows \exists m \in \mathbb{B} \arrowvert f(x) \ge m \, , \forall \,  x\in \mathbb{A}$

\subsection{Funzioni Monotone Crescenti}
Una funzione $f: \mathbb{A}\subset \mathbb{R} \rightarrow \mathbb{B} \subset \mathbb{R}$ si dice \textbf{monotona crescente}

 $\rightleftarrows \forall x_1,x_2 \in \mathbb{A} \arrowvert \, x_1<x_2$ allora $f(x_2) > f(x_1)$
 
 \subsection{Funzioni Monotone Decrescenti}
 Una funzione $f: \mathbb{A}\subset \mathbb{R} \rightarrow \mathbb{B} \subset \mathbb{R}$ si dice \textbf{monotona decrescente}
 
 $\rightleftarrows \forall x_1,x_2 \in \mathbb{A} \arrowvert \, x_1<x_2$ allora $f(x_1) > f(x_2)$
 
 \subsection{Funzioni Algebriche}
 Una funzione $f: \mathbb{A}\subset \mathbb{R} \rightarrow \mathbb{B} \subset \mathbb{R}$ si dice \textbf{algebrica} se è esclusivamente costituita da un numero finito di applicazioni delle 4 operazioni elementari o elevamento a potenza
 
 \subparagraph{Esempio:}
 $y = x^3 -2x^2 - \frac{4}{3}x +\sqrt 2$ \textbf{è una funzione algebrica}
 
 \subsection{Funzioni Trascendenti}
 Una funzione $f: \mathbb{A}\subset \mathbb{R} \rightarrow \mathbb{B} \subset \mathbb{R}$ si dice \textbf{trascendente} se essa \textbf{\underline{non}} è algebrica.
 
 Le funzioni trascendenti generalmente contengono espressioni goniometriche, logaritmiche o esponenziali; tuttavia una funzione contenete una di queste espressioni \textbf{\underline{non} è detto che sia trascendente}.
 
\subparagraph{Esempio:} La funzione $y = x^3 +\cos^2(x) + \sin^2(x) = 0$ \textbf{\underline{non} è trascendente.}

Infatti, sapendo che $\cos^2(x) + \sin^2(x) = 1$, la funzione diventa: $y = x^3+1$, ovvero una \textbf{funzione algebrica}. 

\subsection{Funzioni Periodiche} 
Una funzione $f: \mathbb{A}\subset \mathbb{R} \rightarrow \mathbb{B} \subset \mathbb{R}$ si dice \textbf{periodica} di periodo $T$:

$\leftrightarrows f(x + T) = f(x)$.

\subsubsection{Esempi di funzioni Periodiche}
\begin{itemize}
	\item $cos(x)$ è una funzione periodica con periodo $T = 2k\pi, \, \forall k \in \mathbb{R}$, infatti $cos(\frac{\pi}{2} +4\pi) = \cos(\frac{\pi}{2}) = 1$
	\item $sen(x)$ è una funzione periodica con periodo $T = 2k\pi, \, \forall k \in \mathbb{R}$
	
	\item $tan(x)$ è una funzione periodica con periodo $T = k\pi, \, \forall k \in \mathbb{R}$.
\end{itemize}


\subsection{Funzioni Trigonometriche}
Sono tutte le tipologie di funzioni che hanno come argomento un angolo $\theta$

\subsubsection{Funzione Coseno}
Dato un angolo $\theta$ sulla circonferenza goniometrica, il \textbf{coseno dell'angolo $\theta$} è la trasposizione, lungo l'asse delle x, dell'ipotenusa del triangolo rettangolo inscritto nella circonferenza.

\begin{multicols}{2}
		\begin{tikzpicture}[scale=1.25]
	\title{cos}
	\filldraw[fill=green!20,draw=green!50!black] (0,0) -- (3mm,0mm) arc (0:30:3mm) -- cycle; 
	\draw[->] (-1.25,0) -- (1.25,0) coordinate (x axis);
	\draw[->] (0,-1.25) -- (0,1.25) coordinate (y axis);
	\draw (0,0) circle (1cm);
	\draw[very thick,dashed] (30:1cm) -- (30:1cm |- x axis);
	\draw[very thick,blue] (30:1cm |- x axis) -- node[below=2pt,fill=white] {$\cos \theta$} (0,0);
	\draw (0,0) -- (30:1cm);
	\foreach \x/\xtext in {-1, -0.5/-\frac{1}{2}, 1} 
	\draw (\x cm,1pt) -- (\x cm,-1pt) node[anchor=north,fill=white] {$\xtext$};
	\foreach \y/\ytext in {-1, -0.5/-\frac{1}{2}, 0.5/\frac{1}{2}, 1} 
	\draw (1pt,\y cm) -- (-1pt,\y cm) node[anchor=east,fill=white] {$\ytext$};
	\end{tikzpicture}
	
	L'immagine qui sopra mostra il significato goniometrico del coseno di un angolo.
	
	\columnbreak
	\begin{tikzpicture}	
	\begin{axis}[
	axis lines = center,
	xlabel = $x$,
	ylabel = $f(x)$,
	xtick={-1.6,1.6},
	xticklabels={$\frac{3}{2}\pi$, $\frac{\pi}{2}$}
	]
	%Below the red parabola is defined
	\addplot [
	domain=-pi:pi,
	samples=100, 
	color=blue,
	]
	{cos(deg(x))};
	\end{axis}
	\end{tikzpicture}
	
	La figura qui sopra mostra l'andamento della \textbf{\underline{funzione}}
	 
	 $y = cos(x)$ 
	

\end{multicols}
\subparagraph{Attenzione:} E' necessario tenere a mente la distinzione tra $cos(\theta)$ e $y=cos(x)$; solo nel secondo caso, infatti, si puo' parlare di dominio e codominio in quanto è una funzione.
La prima, invece, è semplicemente una costante numerica.

\subsubsection{Funzione Seno}
Dato un angolo $\theta$ sulla circonferenza goniometrica, \textbf{il seno dell'angolo $\theta$} è la trasposizione, lungo l'asse delle y, dell'ipotenusa del triangolo rettangolo inscritto nella circonferenza.

\pagebreak

\begin{multicols}{2}
	 \begin{tikzpicture}[scale=1.25]
	\filldraw[fill=green!20,draw=green!50!black] (0,0) -- (3mm,0mm) arc (0:30:3mm) -- cycle; 
	\draw[->] (-1.25,0) -- (1.25,0) coordinate (x axis);
	\draw[->] (0,-1.25) -- (0,1.25) coordinate (y axis);
	\draw (0,0) circle (1cm);
	\draw[very thick,red] (30:1cm) -- node[right,fill=white] {$\sin \theta$} (30:1cm |- x axis);
	\draw[very thick] (30:1cm |- x axis) --  (0,0);
	\draw (0,0) -- (30:1cm);
	\foreach \x/\xtext in {-1, -0.5/-\frac{1}{2}, 1} 
	\draw (\x cm,1pt) -- (\x cm,-1pt) node[anchor=north,fill=white] {$\xtext$};
	\foreach \y/\ytext in {-1, -0.5/-\frac{1}{2}, 0.5/\frac{1}{2}, 1} 
	\draw (1pt,\y cm) -- (-1pt,\y cm) node[anchor=east,fill=white] {$\ytext$};
	\end{tikzpicture}
	
	L'immagine qui sopra mostra il significato goniometrico del seno di un angolo
	
	\columnbreak
	
	\begin{tikzpicture}
	\begin{axis}[
	title={$y=\sin(x)$ (dispari)},
	axis lines = center,
	xtick={1.55,3.1,-3.1,-1.55},
	xticklabels={$\frac{\pi}{2}$,$2\pi$, $-\pi$ , $-\frac{\pi}{2}$},
	xlabel = $x$,
	ylabel = $f(x)$,
	]
	%Below the red parabola is defined
	\addplot [
	domain=-pi:pi,
	samples=100, 
	color=red,
	]
	{sin(deg(x))};
	\end{axis}
	\end{tikzpicture}
	
	La figura qui sopra mostra l'andamento della \textbf{\underline{funzione}} 
	
	$y = \sin(x)$
	
	
\end{multicols}

\subsubsection{Funzione Tangente}
Dato un angolo $\theta$, sulla circonferenza goniometrica, la funzione \textbf{tangente} è definita come il rapporto tra seno e coseno di un angolo:


$\tan(\theta) = \frac{\sin(\theta)}{\cos(\theta)}$

\begin{multicols}{2}
	 \begin{tikzpicture}[scale=1.25]
	\filldraw[fill=green!20,draw=green!50!black] (0,0) -- (3mm,0mm) arc (0:30:3mm) -- cycle; 
	\draw[->] (-1.25,0) -- (1.25,0) coordinate (x axis);
	\draw[->] (0,-1.25) -- (0,1.25) coordinate (y axis);
	\draw (0,0) circle (1cm);
	\draw (1cm,0) -- (1cm,1cm);
	\draw[very thick, orange] (1cm,0) -- node[right]{$\tan(\theta)$} (1cm,0.6cm);
	\draw[very thick] (30:1cm |- x axis) --  (0,0);
	\draw (0,0) -- (30:1.16cm);
	\foreach \x/\xtext in {-1, -0.5/-\frac{1}{2}, 1} 
	\draw (\x cm,1pt) -- (\x cm,-1pt) node[anchor=north,fill=white] {$\xtext$};
	\foreach \y/\ytext in {-1, -0.5/-\frac{1}{2}, 0.5/\frac{1}{2}, 1} 
	\draw (1pt,\y cm) -- (-1pt,\y cm) node[anchor=east,fill=white] {$\ytext$};
	\end{tikzpicture}
	
	
	
	\columnbreak
	

\begin{tikzpicture}
\begin{axis}[
axis lines=middle,
xmin=-3.14,xmax=3.14,ymin=-4.5,ymax=4.5,
xlabel={$x$},
ylabel={$f(x)$},
xtick={-1.55,1.55},
xticklabels={$-\frac{\pi}{2}$,$\frac{\pi}{2}$}
]
\addplot[orange,samples=200] {tan(deg(x))};
\end{axis}

\end{tikzpicture}
\end{multicols}

\subsubsection{Funzioni Trigonometriche Inverse}
Le funzioni trigonometriche inverse non hanno una definizione matematica ben definita, ma di esse si possono calcolare i valori con un ragionamento logico.
\begin{itemize}
	\item $\arccos(x)$ è "l'arco il cui coseno è x"
		\subparagraph{Esempio}: $\arccos(1) = \frac{\pi}{2} +2k\pi, con \, k \in \mathbb{R}$, infatti $\cos(\frac{\pi}{2} + 2k\pi) = 1$
		
	\item $\arcsin(x)$ è "l'arco il cui seno è x"
	\item $\arctan(x)$
\end{itemize}

\subsubsection{Funzione Trigonometriche Derivate}
In questa piccola sezione verranno indicate, in maniera non approfondita, tutte le altre funzioni che si ricavano dalle principali.
\begin{multicols}{2}
	\subparagraph{Cosecante}: La funzione \textbf{cosecante} è definita come:
	
	$\csc(x) = \frac{1}{\sin(x)}$
	
	\columnbreak
	\subparagraph{Secante}: La funzione \textbf{secante} è definita come:
	
	$\sec(x) = \frac{1}{\cos(x)}$
\end{multicols}

\begin{center}
\item	\subparagraph{Cotangente}: La funzione \textbf{cotangente} è definita come:
	
	$\cot(x) = \tan^{-1}(x) = \frac{\cos(x)}{\sin(x)}$
\end{center}

\subsubsection{Angoli Fondamentali Trigonometria}
\begin{center}
	\begin{tabular}{|c|c|c|c|c|c|c|c|}
		\hline
		gradi & 0 $\equiv$ 360 & 30 & 45 & 60 & 90 & 180 & 270 \\
		\hline
		radianti & 0 $\equiv$ $2\pi$ & $\frac{\pi}{6}$ & $\frac{\pi}{4}$ & $\frac{\pi}{3}$ & $\frac{\pi}{2}$ & $\pi$ & $\frac{3}{2}\pi$ \\
		\hline
	\end{tabular}
\end{center}

\subsubsection{Valori delle Funzioni Trigonometriche per gli Angoli Notevoli}

\begin{center}
	\begin{tabular}{|c|c|c|c|c|c|c|c|}
		\hline
		angoli & 0$\equiv \, 2\pi$ & $\frac{\pi}{6}$ & $\frac{\pi}{4}$ & $\frac{\pi}{3}$  & $\frac{\pi}{2}$ & $\pi$ & $\frac{3}{2}\pi$ \\
		\hline
		$\sin(x)$ & 0 & $\frac{1}{2}$ &$\frac{\sqrt{2}}{2}$ & $\frac{\sqrt{3}}{2}$ & 1 & 0& -1 \\
		\hline
		$\cos(x)$ & 1 & $\frac{\sqrt{3}}{2}$ & $\frac{\sqrt{2}}{2}$ & $\frac{1}{2}$ & 0 & -1& 0 \\
		\hline
		$\tan(x)$ & 0 & $\frac{\sqrt{3}}{3}$ & 1 & $\sqrt{3}$ & $\pm\infty$ & 0 & $\pm\infty$ \\
		\hline
	\end{tabular}
\end{center}

\subsubsection{Formule Fondamentali Trigonometria}
\begin{enumerate}
	\item Relazione fondamentale della trigonometria: $cos^2(x) + sin^2(x) = 1$
	
	\item Formule di addizione:
		\subitem $\sin(\alpha + \beta) = \sin(\alpha)\cos(\beta) + \sin(\beta)\cos(\alpha)$
		\subitem $\cos(\alpha + \beta) = \cos(\alpha)\cos(\beta) - \sin(\alpha)\sin(\beta)$
	
	\item Formule di sottrazione:
		\subitem  $\sin(\alpha - \beta) = \sin(\alpha)\cos(\beta) - \sin(\beta)\cos(\alpha)$
		\subitem $\cos(\alpha - \beta) = \cos(\alpha)\cos(\beta) + \sin(\alpha)\sin(\beta)$
	\item Formule di duplicazione:
		\subitem $\sin(2\alpha) = \sin(\alpha + \alpha)$
		\subitem $\cos(2\alpha) = \cos(\alpha + \alpha)$
		
	\item Formule di bisezione (i segni cambiano a seconda del quadrante al quale $\frac{\alpha}{2}$ appartiene):
		\subitem $\cos(\frac{\alpha}{2}) = \pm \sqrt{\frac{1 + \cos(\alpha)}{2}}$
		
		\subitem $\sin(\frac{\alpha}{2}) = \pm \sqrt\frac{1-\cos(x)}{2}$
		
		\subitem $\tan(\frac{\alpha}{2}) = \pm \sqrt{\frac{1-\cos(\alpha)}{1+\cos(\alpha)}}$
		
	\item Formule parametriche
		\subitem $\cos(\alpha) = \frac{1-t^2}{1+t^2}$
		\subitem $\sin(\alpha) = \frac{2t}{1+t^2}$
		\subitem $\tan(\alpha) = \frac{2t}{1-t^2}$
		
		ove $t = \tan(\frac{\alpha}{2})$
		
		\subparagraph{NB:}
		Queste sono formule che escono molto di rado; tuttavia è sempre meglio conoscerle.
		
\end{enumerate}

\subsection{Funzione Esponenziale}
Le funzioni esponenziali sono del tipo:

$y = a^x,$ ove $a \in \mathbb{R}$

La funzione esponenziale si comporta in due modi diversi:
\begin{itemize}
	\item $y$ è \textbf{monotona crescente} $\leftrightarrows \, a > 1$
	\item $y$ è \textbf{monotona decrescente} $\leftrightarrows \, 0<a<1$
\end{itemize}

\begin{multicols}{2}
	\begin{tikzpicture}
	\begin{axis}[
	axis lines=middle,
	xlabel={$x$},
	ylabel={$f(x)$},
	]
	\addplot[brown,samples=200] {exp(x)};
	\end{axis}
	
	\end{tikzpicture}
	\begin{center}
		$y = e^x$
	\end{center}
	
	
	\columnbreak
	
	\begin{tikzpicture}
	\begin{axis}[
	axis lines=middle,
	xlabel={$x$},
	ylabel={$f(x)$},
	]
	\addplot[brown,samples=100] {(1/2)^x};
	\end{axis}
	\end{tikzpicture}
	
	\begin{center}
		$y = \frac{1}{2}^x$
	\end{center}
	
\end{multicols}

\subsubsection{Prorpietà delle potenze}
Per affrontare le funzioni esponenziali, è necessario avere bene a mente le proprietà delle potenze, pertanto in questo breve capitolo verranno elencate, in modo non approfondito, le proprietà delle potenze:

\begin{enumerate}
	\item $x^a * x^b = x^{a+b}$
	\item $x^a / x^b = \frac{x^a}{x^b} = x^{a-b}$
	\item $(x^a)^b = x^{ab}$
	\item $x_1^a * x_2^a = (x_1*x_2)^a$
	\item $x_1^a / x_2^a = \frac{x_1^a}{x_2^a} = (\frac{x_1}{x_2})^a$
\end{enumerate}

\subsubsection{Prodotti notevoli}
Per non farci mancare niente, ricapitoliamo anche i prodotti notevoli

\begin{enumerate}
	\item $(a \pm b)^2 = a^2 \pm 2ab + b^2 $
	\item $(a+b)(a-b) = a^2-b^2$
	\item $(a+b+c)^2 = a^2+b^2+c^2 +2ab +2bc + 2ac$
	\item $(a \pm b)^3 = a^3 \pm 3a^2b \pm 3ab^2 \pm b^3$
	\item $a^3 \pm b^3 = (a \pm b)(a^2 \mp ab +b^2)$
\end{enumerate} 

\subsection{Funzioni logaritmiche}
La funzione logaritmica è l'inverso della funzione esponenziale ed è definita come:

$ y = \log_a(b)$

Un modo semplice per calcolarne il risultato è ricordarsi che il logaritmo è \textbf{l'esponente da dare  ad $a$ per ottenere  $b$}.

\subparagraph{Esempio:} 
$\log_{10} (100) = 2$, infatti $10^2 = 100$


La funzione logaritmica si comporta (in maniera equivalente alle esponenziali) in due modi diversi:
\begin{itemize}
	\item $y$ è \textbf{monotona crescente} $\leftrightarrows \, a > 1$
	\item $y$ è \textbf{monotona decrescente} $\leftrightarrows \, 0<a<1$
\end{itemize}

\begin{multicols}{2}
	\begin{tikzpicture}
	\begin{axis}[
	axis lines=middle,
	xlabel={$x$},
	ylabel={$f(x)$},
	]
	\addplot[brown,samples=200] {ln(x)};
	\end{axis}
	
	\end{tikzpicture}
	\begin{center}
		$y = ln(x) = \log_e(x)$
	\end{center}
	
	
	\columnbreak
	
	\begin{tikzpicture}
	\begin{axis}[
	axis lines=middle,
	xlabel={$x$},
	ylabel={$f(x)$},
	]
	\addplot[brown,samples=100] {ln(x)/ln(1/2)};
	\end{axis}
	\end{tikzpicture}
	
	\begin{center}
		$y = \log_{\frac{1}{2}}(x)$
	\end{center}
	
\end{multicols}

\subsubsection{Proprietà dei logaritmi}
In questo piccolo capitolo ricapitoleremo, in modo non approfondito, le proprietà dei logaritmi da tenere a mente.
\begin{enumerate}
	\item $a^{\log_a(x)} = x$
	\item $\log_a(a^x) = x$
	\item $\log_a(x_1x_2) = \log_a(x_1) + \log_a(x_2)$
	\item $\log_a(\frac{x_1}{x_2}) = \log_a(x_1) - \log_a(x_2)$
	\item $\log_a(x^b) = b\log_a(x)$
\end{enumerate}

\subsubsection{Formula per il cambio di base del logaritmo}
Avendo un logaritmo generico in una base generica, è possibile effettuare il cambiamento di base mediante la seguente formula:

$\log_a(x) = \frac{\log_c(x)}{\log_c(a)}$

\subparagraph{Suggerimento:} Per ricordare questa formula, è sufficiente notare che la $x$ sta (nella scrittura del logaritmo) "sopra" e quindi in seguito sarà al numeratore, mentre la $a$ è posizionata al pedice e infatti si collocherà al denominatore



\subsection{Domini di Funzioni Algebriche}
In questa sezione verranno elencati i domini delle funzioni algebriche, cercando di analizzare anche il procedimento con il quale essi vengono ricavati.

\subsubsection{Come trovare il dominio} 
Facendo riferimento alle nozioni teoriche enunciate ad inizio capitolo, il \textbf{Dominio} è definito come l'insieme di \textbf{"partenza"}.

Gli elementi che appartengono al dominio sono tutti quei valori che verranno elaborati dalla funzione per farli giungere nell'insieme d'\textbf{"arrivo"}.

Avendo ben chiari questi concetti, risulta lampante che \textbf{\underline{tutti}} gli elementi del dominio, devono avere un immagine tramite $f$.

Perciò è necessario \textbf{\underline{escludere}}, dal dominio, tutti quei valori "di fastidio" che non hanno un immagine tramite $f$

\subsubsection{Dominio di un polinomio di grado n}
Iniziamo con il testare il nostro procedimento che ci permette di individuare il dominio di una funzione.

La funzione presa in considerazione in questa sottosezione è: 

$y = a_nx^n + a_{n-1}x^{n-1}+ \cdots + a_2x^2+ax+c$

La domanda da porsi è: \textbf{C'è qualche valore di x che puo' rendere impossibile la risoluzione della funzione (che da appunto "fastidio")?}

Dall'algebra elementare noi sappiamo \textbf{sempre} calcola il valore di un polinomio di grado n, pertanto non esistono dei valori di $x$ fastidiosi, e perciò il dominio di questa funzione è:

$\mathbb{D}: \forall x \in \mathbb{R}$ oppure $\mathbb{D}: \mathbb{R}$

\subsubsection{Dominio funzione razionale}
Una funzione razionale fratta è una funzione del tipo:

$y = \frac{N(x)}{D(x)}$

ove:

\begin{itemize}
	\item $N(x)$ rappresenta un polinomio di grado n, ed è il \textbf{numeratore}.
	\item $D(x)$ rappresenta un polinomio di grado n, ed è il \textbf{denominatore}.
\end{itemize}

In una funzione di questo tipo è presente un problema che denota una variazione del dominio della funzione (che non sarà più tutto $\mathbb{R}$).

Fin dalle elementari, infatti, ci è stato insegnato che \textbf{\underline{NON} è possibile effettuare una divisione per 0}.

Pertanto è necessario escludere dal dominio tutte le radici (soluzioni) dell'equazione $D(x) \neq 0$

\subparagraph{Esempio:} Supponiamo di avere la funzione:

$y = \frac{5x^2 -3}{x^2-1}$

Determiniamo il dominio, come abbiamo detto:

$D(x) \neq 0$, ovvero $x^2 -1 \neq 0 \leftrightarrows x \neq \pm 1$

\end{document}